package br.sc.rafael.pages;

import static br.sc.rafael.core.DriverFactory.getDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;

import br.sc.rafael.core.BasePage;

public class AdicionarClientePage extends BasePage {
	
	public void setComboV4(String valor) {
		selecionarCombo("switch-version-select", valor);
		
	}
	
	public void clicarBotaoAddCliente() {
		clicarBotao(By.xpath("//div[@class='floatL t5']/a[@class='btn btn-default btn-outline-dark']"));
	}
	
	public void setNome(String texto) {
		escrever("field-customerName", texto);
	}
	
	public void setSobrenome(String texto) {
		escrever("field-contactLastName", texto);
	}
	
	public void setContatoPrimeiroNome(String texto) {
		escrever("field-contactFirstName", texto);
	}
	
	public void setFone(String texto) {
		escrever("field-phone", texto);
	}
	
	public void setEnderecoUm(String texto) {
		escrever("field-addressLine1", texto);
	}
	
	public void setEnderecoDois(String texto) {
		escrever("field-addressLine2", texto);
	}
	
	public void setCidade(String texto) {
		escrever("field-city", texto);
	}
	
	public void setEstado(String texto) {
		escrever("field-state", texto);
	}
	
	public void setCodigoPostal(String texto) {
		escrever("field-postalCode", texto);
	}
	
	public void setPais(String texto) {
		escrever("field-country", texto);
	}
	
	public void setEmpregador(String valor) {
		//selecionarCombo(By.xpath("//div[@class='chosen-container chosen-container-single chosen-container-active']"), valor);
		//selecionarCombo(By.xpath("//select[@class='chosen-container chosen-container-single']"), valor);
		selecionarCombo("field_salesRepEmployeeNumber_chosen", valor); 
		

	}					 	
	
	public void setLimiteCredito(String texto) {
		escrever("field-creditLimit", texto);
	}
	
	public void clicarBotaoSalvar() {
		clicarBotao("form-button-save");
	}
	
	public void rolarScrolDown() {
		
		JavascriptExecutor js = ((JavascriptExecutor) getDriver());
		js.executeScript("window.scrollTo(0, document.body.scrollHeight)");
		//Thread.sleep(5000);
	}
	
	//Asserts
	public String obterNomeCliente() {
		return obterValorCampo("field-customerName");
	}
	
	public String obterSobrenomeCliente() {
		return obterValorCampo("field-contactLastName");
	}
	
	public String obterContatoPrimeiroNome() {
		return obterValorCampo("field-contactFirstName");
	}
	
	public String obterFone() {
		return obterValorCampo("field-phone");
	}
	
	public String obterEnderecoUm() {
		return obterValorCampo("field-addressLine1");
	}
	
	public String obterEnderecoDois() {
		return obterValorCampo("field-addressLine2");
	}
	
	public String obterCidade() {
		return obterValorCampo("field-city");
	}
	
	public String obterEstado() {
		return obterValorCampo("field-state");
	}
	
	public String obterCodigoPostal() {
		return obterValorCampo("field-postalCode");
	}
	
	public String obterPais() {
		return obterValorCampo("field-country");
	}
	
	public String obterEmpregador() {
		return obterValorCampo("field-salesRepEmployeeNumber");
	}
	
	public String obterLimiteCredito() {
		return obterValorCampo("field-creditLimit");
	}

}
