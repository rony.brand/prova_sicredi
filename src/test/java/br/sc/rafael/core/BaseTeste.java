package br.sc.rafael.core;

import static br.sc.rafael.core.DriverFactory.killDriver;

import org.junit.After;
import org.junit.Before;

import br.sc.rafael.pages.AcessoSite;

public class BaseTeste {
	
	AcessoSite acessoSite = new AcessoSite();
	
	@Before
	public void inicializaSite() {
		acessoSite.acessarTelaPrincipal();
	}
	
	
	@After
	public void finaliza() {
		
		if(Propriedades.FECHAR_BROWSER) {
			killDriver();
		}		
	}
}
