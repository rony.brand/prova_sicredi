package br.sc.rafael.tests;

import org.junit.Assert;
import org.junit.Test;

import br.sc.rafael.core.BaseTeste;
import br.sc.rafael.pages.AdicionarClientePage;


public class AdicionarClienteTest extends BaseTeste {
	
	AdicionarClientePage addCliente = new AdicionarClientePage();
	
	@Test
	public void addClienteComSucesso() {
		addCliente.setComboV4("Bootstrap V4 Theme");
		//colocar um assert depois
		addCliente.clicarBotaoAddCliente();
		addCliente.setNome("Teste Sicredi");
		addCliente.setSobrenome("Teste");
		addCliente.setContatoPrimeiroNome("Rafael Teixeira");
		addCliente.setFone("51 9999-9999");
		addCliente.setEnderecoUm("Av Assis Brasil, 3970");
		addCliente.setEnderecoDois("Torre D");
		addCliente.rolarScrolDown();
		addCliente.setCidade("Porto Alegre");
		addCliente.setEstado("RS");
		addCliente.setCodigoPostal("91000-000");
		addCliente.setPais("Brasil");
		addCliente.setEmpregador("Bott");
		addCliente.setLimiteCredito("200");
		
		Assert.assertEquals("Teste Sicredi", addCliente.obterNomeCliente());
		Assert.assertEquals("Teste", addCliente.obterSobrenomeCliente());
		Assert.assertEquals("Rafael Teixeira", addCliente.obterContatoPrimeiroNome());
		Assert.assertEquals("51 9999-9999", addCliente.obterFone());
		Assert.assertEquals("Av Assis Brasil, 3970", addCliente.obterEnderecoUm());
		Assert.assertEquals("Torre D", addCliente.obterEnderecoDois());
		Assert.assertEquals("Porto Alegre", addCliente.obterCidade());
		Assert.assertEquals("RS", addCliente.obterEstado());
		Assert.assertEquals("Teste Sicredi", addCliente.obterNomeCliente());
		Assert.assertEquals("91000-000", addCliente.obterCodigoPostal());
		Assert.assertEquals("Brasil", addCliente.obterPais());
		//Assert.assertEquals("91000-000", addCliente.obterCodigoPostal());
		Assert.assertEquals("200", addCliente.obterLimiteCredito());
			
		addCliente.clicarBotaoSalvar();
		
		
	}
	
}
