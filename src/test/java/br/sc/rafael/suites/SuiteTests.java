package br.sc.rafael.suites;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.sc.rafael.tests.AdicionarClienteTest;

@RunWith(Suite.class)
@SuiteClasses({
	AdicionarClienteTest.class
	
})

public class SuiteTests {

}
